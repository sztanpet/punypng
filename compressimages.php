#!/usr/bin/php
<?php
require('punypng.php');
$path = dirname( __FILE__ );

function generateSprite() {
  global $path;
  
  $command = 'sh -c "spritemapper css/style.css"';
  echo "Launching: ", $command, "\n\n";
  exec( $command, $output, $return );
  echo implode("\n", $output);
  
}

function compressSprite() {
  global $path;
  
  $p = new PunyPNG('');
  $p->addImage( $path . '/images/sprite.png' );
  echo "Optimizing images/sprite.png\n";
  $p->optimize();
  echo "Fetching optimized file from punyPNG\n";
  $p->getOptimizedFiles();
  
}

generateSprite();
echo "\n";
compressSprite();
