<?php

class PunyPNG {
  private $apikey;
  private $apiurl = 'http://www.punypng.com/api/optimize';
  private $request;
  private $responses;
  private $images = array();
  
  public function __construct( $apikey, PunyPNGWebRequestInterface $request = null ) {
    
    $this->apikey = (string)$apikey;
    
    if ( $request )
      $this->request = $request;
    else
      $this->request = new PunyPNGWebRequest();
    
  }
  
  public function addImage( $inimage, $outimage ) {
    
    if ( !is_readable( $inimage ) )
      throw new PunyPNGException( 'Image not readable: ' . $inimage );
    
    if ( !is_writable( dirname( $outimage ) ) )
      throw new PunyPNGException( 'Image not writable: ' . $outimage );
    
    $this->images[ $inimage ] = array(
      'inimage'  => $inimage,
      'outimage' => $outimage,
    );
    
  }
  
  public function setWebRequest( PunyPNGWebRequestInterface $request ) {
    $this->request = $request;
  }
  
  public function optimize() {
    
    $responses = array();
    foreach( $this->images as $image ) {
      
      $data = array(
        'key' => $this->apikey,
        'img' => $image['inimage'],
      );
      
      $response = array(
        'response' => $this->request->call( 'post', $this->apiurl, $data ),
        'outimage' => $image['outimage'],
      );
      
      if ( $response['response']->data === false ) {
        continue;
      }
      
      $responses[ $image['inimage'] ] = $response;
      
    }
    
    $this->responses = new PunyPNGResponseIterator( $responses );
    return $this->responses;
    
  }
  
  public function getOptimizedFiles( PunyPNGResponseIterator $responses = null ) {
    
    if ( !$responses )
      $responses = $this->responses;
    
    foreach( $responses as $response ) {
      
      if ( !$response or empty( $response ) )
        continue;
      
      $request = $this->request->call( 'get', $response['optimized_url'], array(
          'outimage' => $response['outimage'],
        )
      );
      
    }
    
    return true;
    
  }
  
}

class PunyPNGResponseIterator implements Iterator {
  private $current;
  private $position = 0;
  private $length = 0;
  private $responses;
  
  public function __construct( array $responses ) {
    $this->responses = $responses;
    $this->length    = count( $responses );
  }
  
  public function __destruct() {
    unset( $this->current, $this->responses );
  }
  
  public function rewind() {
    
    $this->position = 0;
    
    if ( $this->length )
      $this->current = current( $this->responses );
    
  }
  
  public function valid() {
    
    if ( !$this->length )
      return false;
    
    $i = 0;
    foreach( $this->responses as $response ) {
      
      if ( $i == $this->position )
        return true;
      
      $i++;
      
    }
    
    return false;
    
  }
  
  public function current() {
    
    if ( !$this->current or $this->current['response']->data === false )
      return false;
    
    $data = json_decode( $this->current['response']->data, true );
    
    if ( !$data )
      return false;
    
    $data['outimage'] = $this->current['outimage'];
    return $data;
    
  }
  
  public function key() {
    return $this->current['outimage'];
  }
  
  public function next() {
    
    $this->position++;
    
    $i = 0;
    foreach( $this->responses as $response ) {
      
      if ( $i == $this->position ) {
        
        $this->current = $response;
        break;
        
      }
      
      $i++;
      
    }
    
  }
  
}

interface PunyPNGWebRequestInterface {
  public function call( $method, $url, array $data = null );
}

class PunyPNGWebRequestResult {
  public $httpcode;
  public $data;
  public $error;
  public $errno;
}

class PunyPNGWebRequest implements PunyPNGWebRequestInterface {
  private $curl;
  public $curloptions = array(
    CURLOPT_FAILONERROR    => true,
    CURLOPT_HEADER         => false,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_SSL_VERIFYHOST => 0,
    CURLOPT_CONNECTTIMEOUT => 1,
    CURLOPT_USERAGENT      => 'sztanpet Punypng api client',
  );
  
  public function __construct() {
    
    if ( !extension_loaded('curl') )
      throw new PunyPNGException('Extension curl not available');
    
  }
  
  protected function initCurl( Array $options = null ) {
    
    $options = $options? $options: $this->curloptions;
    $this->curl = curl_init();
    
    if ( !curl_setopt_array( $this->curl, $options ) )
      throw new PunyRequestException("Unable to set an option");
    
    return $this;
    
  }
  
  public function call( $method, $url, array $data = null ) {
    
    $options = $this->curloptions;
    $method  = strtolower( $method );
    
    switch( $method ) {
      
      case 'get':
        
        if ( $data and $data['outimage'] ) {
          
          $handle = fopen( $data['outimage'], 'w+' );
          
          if ( !$handle )
            throw new PunyPNGException('Could not create file handle for: ' . $data['outimage'] );
          
          $options[ CURLOPT_FILE ] = $handle;
          $options[ CURLOPT_RETURNTRANSFER ] = false;
        }
        
        break;
      
      case 'post':
        $options[ CURLOPT_POST ] = true;
        
        if ( $data ) {
          
          if ( $data['img'] )
            $data['img'] = '@' . $data['img'];
          
          $options[ CURLOPT_POSTFIELDS ] = $data;
          
        }
        
        break;
      
      default:
        throw new PunyRequestException('Unknown method passed: ' . $method );
        break;
      
    }
    
    $options[ CURLOPT_URL ] = $url;
    $this->initCurl( $options );
    
    $result           = new PunyPNGWebRequestResult();
    $result->data     = curl_exec( $this->curl );
    $result->httpcode = curl_getinfo( $this->curl, CURLINFO_HTTP_CODE );
    
    if ( $result->data === false ) {
      
      if ( !$result->httpcode ) {
        
        $result->errno = curl_errno( $this->curl );
        $result->error = curl_error( $this->curl );
        
      }
      
    }
    
    curl_close( $this->curl );
    
    if ( isset( $handle ) )
      fclose( $handle );
    
    return $result;
    
  }
  
}

class PunyRequestException extends Exception {}
class PunyPNGException extends Exception {}
